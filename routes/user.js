const CONFIG = require('../config/config');
const UserModel = require('../models/users');

const router = require('koa-router')()

router.prefix('/user')
//获取用户信息
router.get('/getAllUsers',async function(ctx, next){
  let conditions = {};
  const results = await UserModel.find(conditions);
  ctx.send({data:results});
 
})

// 配置get路由
router.get('/get',async function (ctx, next) {
  
    ctx.send({
      status: 'success',
      data: 'hello ikcmap'
    })
})

//获取用户信息
router.post('/addUser',async (ctx, next)=>{
  console.log(ctx)
  console.log(JSON.stringify(ctx.request.body))
  let { name, password, phone, email} = ctx.request.body;
  console.log(name)
    //保存到数据库
    let user = new UserModel({
      email,
      name,
      password,
      phone,
    });
  const results = await  user.save();
  ctx.send({ status: 'success',data:results});
 
})


module.exports = router; 
