/*
 Navicat Premium Data Transfer

 Source Server         : wz
 Source Server Type    : MySQL
 Source Server Version : 50632
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50632
 File Encoding         : 65001

 Date: 18/05/2019 15:04:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, '语文');
INSERT INTO `course` VALUES (2, '数学');

-- ----------------------------
-- Table structure for employee_tbl
-- ----------------------------
DROP TABLE IF EXISTS `employee_tbl`;
CREATE TABLE `employee_tbl`  (
  `id` int(11) NOT NULL,
  `name` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `date` datetime(0) NOT NULL,
  `singin` tinyint(4) NOT NULL DEFAULT 0 COMMENT '登录次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of employee_tbl
-- ----------------------------
INSERT INTO `employee_tbl` VALUES (1, '小明', '2016-04-22 15:25:33', 1);
INSERT INTO `employee_tbl` VALUES (2, '小王', '2016-04-20 15:25:47', 3);
INSERT INTO `employee_tbl` VALUES (3, '小丽', '2016-04-19 15:26:02', 2);
INSERT INTO `employee_tbl` VALUES (4, '小王', '2016-04-07 15:26:14', 4);
INSERT INTO `employee_tbl` VALUES (5, '小明', '2016-04-11 15:26:40', 4);
INSERT INTO `employee_tbl` VALUES (6, '小明', '2016-04-04 15:26:54', 2);

-- ----------------------------
-- Table structure for sc
-- ----------------------------
DROP TABLE IF EXISTS `sc`;
CREATE TABLE `sc`  (
  `scid` int(11) NOT NULL DEFAULT 0,
  `c` int(11) NULL DEFAULT NULL,
  `score` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`scid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, '张三');
INSERT INTO `student` VALUES (2, '李四');

-- ----------------------------
-- Table structure for student_copy1
-- ----------------------------
DROP TABLE IF EXISTS `student_copy1`;
CREATE TABLE `student_copy1`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 482 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of student_copy1
-- ----------------------------
INSERT INTO `student_copy1` VALUES (1, '张三');
INSERT INTO `student_copy1` VALUES (2, '李四');
INSERT INTO `student_copy1` VALUES (4, 'Roninwz');
INSERT INTO `student_copy1` VALUES (48, 'Roninwz');
INSERT INTO `student_copy1` VALUES (70, 'Roninwz');
INSERT INTO `student_copy1` VALUES (97, 'Roninwz');
INSERT INTO `student_copy1` VALUES (143, 'Roninwz');
INSERT INTO `student_copy1` VALUES (240, 'Roninwz');
INSERT INTO `student_copy1` VALUES (247, 'Roninwz');
INSERT INTO `student_copy1` VALUES (278, 'Roninwz');
INSERT INTO `student_copy1` VALUES (360, 'Roninwz');
INSERT INTO `student_copy1` VALUES (364, 'Roninwz');
INSERT INTO `student_copy1` VALUES (371, 'Roninwz');
INSERT INTO `student_copy1` VALUES (416, 'Roninwz');
INSERT INTO `student_copy1` VALUES (427, 'Roninwz');
INSERT INTO `student_copy1` VALUES (481, 'Roninwz');

-- ----------------------------
-- Table structure for student_course
-- ----------------------------
DROP TABLE IF EXISTS `student_course`;
CREATE TABLE `student_course`  (
  `sid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `cid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `score` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`sid`, `cid`) USING BTREE,
  INDEX `cid`(`cid`) USING BTREE,
  CONSTRAINT `student_course_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `student` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `student_course_ibfk_2` FOREIGN KEY (`cid`) REFERENCES `course` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of student_course
-- ----------------------------
INSERT INTO `student_course` VALUES (1, 1, 80);
INSERT INTO `student_course` VALUES (1, 2, 90);
INSERT INTO `student_course` VALUES (2, 1, 90);
INSERT INTO `student_course` VALUES (2, 2, 70);

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `tid` int(11) NOT NULL DEFAULT 0,
  `tname` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`tid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'wz', '123', '15138143273');

-- ----------------------------
-- Table structure for user_new1
-- ----------------------------
DROP TABLE IF EXISTS `user_new1`;
CREATE TABLE `user_new1`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `addCol` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_old
-- ----------------------------
DROP TABLE IF EXISTS `user_old`;
CREATE TABLE `user_old`  (
  `id` int(20) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_old
-- ----------------------------
INSERT INTO `user_old` VALUES (1, 'wz', '123');
INSERT INTO `user_old` VALUES (2, 'wzwww', '123');
INSERT INTO `user_old` VALUES (3, 'mm', '123');
INSERT INTO `user_old` VALUES (4, 'roninwz', '202cb962ac59075b964b07152d234b70');

SET FOREIGN_KEY_CHECKS = 1;
