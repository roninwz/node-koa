const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const config = require('./config/config');
const app = new Koa();




const middleware = require('./middleware')
 
middleware(app)


app.use(bodyParser());  // 解析request的body




// mongo数据库连接
const mongodb = require('./config/mongodb');
mongodb.connect();


//路由
const registerRouter  = require('./routes')
app.use(registerRouter())

// app.use(router.routes());
app.listen(config.server.port);
console.log('app started at port '+config.server.port)