/**
 * User model module.
 */

const uuid = require('uuid');
const { mongoose } = require('../config/mongodb.js');
const userSchema = new mongoose.Schema({

  id:{type:String,default:uuid.v4},

  // 名字
  name: { type: String, required: true, default: '' },


  // 手机
  phone: { type: String, default: '' },


  // 邮箱
  email: {
    type: String,
    default: '',
    // required: true,
    // validate: /\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/,
  },


  // 密码
  password: {
    type: String,
    required: true,
    default: 123456
  },

  // 创建日期
  create_time: { type: Date, default: Date.now },

  // 最后修改日期
  update_time: { type: Date, default: Date.now },
});


module.exports = mongoose.model('User', userSchema);
